# A16_Team_17_Ride_Pal

RidePal web application for generating playlists. 
Enables users to generate playlist for specific travel duration periods based on their preferred genres.

A playlist consists of a list of individual tracks. Each track has an artist, title, album, duration and rank. Tracks have a preview URL to an audio stream.

Each playlist has a user given title, musical genres, a list of tracks with the associated track details, total playtime and rank.

RidePal calculates the travel duration time between the starting and destination locations and combines tracks
chosen randomly in the specific genres, until the playtime matches the travel time duration. Playlist is saved under user's profile.

The public part of the application is visible withouth authentication. This includes the start page, the user login and user registration forms,
list of all generated playlists. People can only browse the playlists and see the tracks list and details of them.

The private part is accessible only to users who have successfully authenticated (registered). Once logged successfully, users can generate
new playlists, control the generation algorithm, edit or delete their own existing playlists. Editing is limited to changing the title or genres.

System administrators can edit or delete users and other administrators. Also can edit or delete existing playlists. 
Also can generate new tracks in database for existing genre.

Data is stored in a MariaDB relational database.
The application consume three public REST services in order to achieve the main functionality.
   - Microsoft Bing Maps.
   - Deezer.
   - Pixabay.
Project is tiered in 3 main layers - repository, service, and UI. 
Used SpringMVC and SpringBoot framework.
Used Hibernate/JPA in repository layer.
Used Spring Security to handle user registration and user roles (regular user and administrator).

Database consists of more than 3000 tracks and their albums and artists. Each genres are fetched also.

Generation Algorithm - for two starting and destination locations, Bing Maps calculates the time and we should generate brand new playlist.
By default we should not repeat artists (unless “allow tracks from the same artists” is selected). 
By default we should not repeat tracks (unless "use top ranks" is selected).

Pixabay REST service attach randomly images related to music to newly generated playlists.

Gitlab project link - https://gitlab.com/teamwork_team_17/a16_team_17_ridepal_playlist_generator
Trello link - https://trello.com/b/yHTZNcbA/teamworkridepalwebapplication
Link to database scheme - https://my.vertabelo.com/public-model-view/ARqAFcOojMZNqkn2sNU6cLbRNO7gmWBYPQBt2cMdZk4sW1M9STR0l9bNRQympl9S?x=4249&y=4385&zoom=0.6378