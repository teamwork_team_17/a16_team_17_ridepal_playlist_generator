package com.telerikacademy.ridepal.models;

import com.telerikacademy.ridepal.validation.FieldMatch;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@FieldMatch.List({
        @FieldMatch(first = "newPassword", second = "confirmNewPassword", message = "The password fields must match")
})
public class ChangePassword {
    @NotNull(message = "is required")
    @Size(min = 8, max = 30, message = "Password should be between 8 and 30 symbols.")
    private String oldPassword;

    @NotNull(message = "is required")
    @Size(min = 8, max = 30, message = "Password should be between 8 and 30 symbols.")
    private String newPassword;

    @NotNull(message = "is required")
    private String confirmNewPassword;

    public ChangePassword() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }
}
