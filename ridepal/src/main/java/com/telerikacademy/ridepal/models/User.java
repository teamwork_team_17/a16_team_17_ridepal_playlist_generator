package com.telerikacademy.ridepal.models;

import com.telerikacademy.ridepal.validation.FieldMatch;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@FieldMatch.List({
        @FieldMatch(first = "password", second = "confirmPassword", message = "The password fields must match")
})
public class User {
    @NotNull(message = "is required")
    @NotBlank(message = "Username should not be empty")
    @Size(min = 5, max = 20, message = "Username should be between 5 and 20 symbols.")
    private String username;

    @NotNull(message = "is required")
    @Size(min = 8, max = 30, message = "Password should be between 8 and 30 symbols.")
    private String password;

    @NotNull(message = "is required")
    private String confirmPassword;

    @NotNull(message = "is required")
    @Size(min = 3, max = 30, message = "First name should be between 3 and 30 symbols.")
    private String firstName;

    @NotNull(message = "is required")
    @Size(min = 3, max = 30, message = "Last Name should be between 3 and 30 symbols.")
    private String lastName;

    @NotNull(message = "is required")
    private String email;

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
