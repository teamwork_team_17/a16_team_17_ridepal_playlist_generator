package com.telerikacademy.ridepal.models;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class PlaylistDtoEdit {
    @NotNull
    @NotBlank(message = "Title must not be blank")
    @Size(min = 2, max = 20, message = "Title should be between 2 and 20 symbols.")
    private String title;

    private List<Integer> genreList;

    public PlaylistDtoEdit() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Integer> getGenreList() {
        return genreList;
    }

    public void setGenreList(List<Integer> genreList) {
        this.genreList = genreList;
    }
}
