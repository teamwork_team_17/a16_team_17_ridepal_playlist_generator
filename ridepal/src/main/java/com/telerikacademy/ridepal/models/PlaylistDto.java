package com.telerikacademy.ridepal.models;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;

public class PlaylistDto {

    @NotNull
    @NotBlank(message = "Title must not be blank")
    @Size(min = 2, max = 20, message = "Title should be between 2 and 20 symbols.")
    private String title;

    @PositiveOrZero(message = "UserId should be positive or zero")
    private Long userId;

    private boolean allowSameArtists;

    private boolean useTopRankedTracks;

    private List<Integer> genres;

    private List<Integer> percents;

    @NotNull
    @NotBlank(message = "Enter country")
    private String fromCountry;

    @NotNull
    @NotBlank(message = "Enter country")
    private String fromCity;

    @NotNull
    @NotBlank(message = "Enter country")
    private String fromAddress;

    @NotNull
    @NotBlank(message = "Enter country")
    private String toCountry;

    @NotNull
    @NotBlank(message = "Enter country")
    private String toCity;

    @NotNull
    @NotBlank(message = "Enter country")
    private String toAddress;

    public PlaylistDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean isAllowSameArtists() {
        return allowSameArtists;
    }

    public void setAllowSameArtists(boolean allowSameArtists) {
        this.allowSameArtists = allowSameArtists;
    }

    public boolean isUseTopRankedTracks() {
        return useTopRankedTracks;
    }

    public void setUseTopRankedTracks(boolean useTopRankedTracks) {
        this.useTopRankedTracks = useTopRankedTracks;
    }

    public List<Integer> getGenres() {
        return genres;
    }

    public void setGenres(List<Integer> genres) {
        this.genres = genres;
    }

    public List<Integer> getPercents() {
        return percents;
    }

    public void setPercents(List<Integer> percents) {
        this.percents = percents;
    }

    public String getFromCountry() {
        return fromCountry;
    }

    public void setFromCountry(String fromCountry) {
        this.fromCountry = fromCountry;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToCountry() {
        return toCountry;
    }

    public void setToCountry(String toCountry) {
        this.toCountry = toCountry;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }
}
