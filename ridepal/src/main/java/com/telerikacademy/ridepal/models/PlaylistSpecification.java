package com.telerikacademy.ridepal.models;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public final class PlaylistSpecification {
    public static Specification<Playlist> filterByTitle(String expression) {
        return new Specification<Playlist>() {
            @Override
            public Predicate toPredicate(Root<Playlist> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                if(expression ==  null||expression.isEmpty()){
                    Predicate predicate = criteriaBuilder.conjunction();
                    return predicate;
                }else {
                    Predicate equalPredicate = criteriaBuilder.like(criteriaBuilder.lower(root.get("title")), "%" + expression.toLowerCase() + "%");
                    return equalPredicate;
                }
            }
        };
    }

    public static Specification<Playlist> filterByDurationFrom(Integer from) {
        return new Specification<Playlist>() {
            @Override
            public Predicate toPredicate(Root<Playlist> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                if(from==null){
                    Predicate predicate = criteriaBuilder.conjunction();
                    return predicate;
                }else {
                    Predicate predicate = criteriaBuilder.greaterThan(root.get("playtime"), from);
                    return predicate;
                }
            }
        };
    }

    public static Specification<Playlist> filterByDurationTo(Integer to) {
        return new Specification<Playlist>() {
            @Override
            public Predicate toPredicate(Root<Playlist> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

                if(to==null|| to == 0){
                    Predicate predicate = criteriaBuilder.conjunction();
                    return predicate;
                }else {
                    Predicate predicate = criteriaBuilder.lessThan(root.get("playtime"), to);
                    return predicate;
                }
            }
        };
    }

    public static Specification<Playlist> getPlaylistByGenres(String name) {
        return (root, query, criteriaBuilder) -> {
            if(name == null||name.isEmpty()) {
                Predicate predicate = criteriaBuilder.conjunction();
                return predicate;
            }
            Join<Object, Object> phoneJoin = root.join("genres");
            return criteriaBuilder.equal(phoneJoin.get("name"), name);
        };
    }
}
