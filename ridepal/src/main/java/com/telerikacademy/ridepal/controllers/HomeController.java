package com.telerikacademy.ridepal.controllers;

import com.mysql.cj.exceptions.WrongArgumentException;
import com.telerikacademy.ridepal.models.Genre;
import com.telerikacademy.ridepal.models.Playlist;
import com.telerikacademy.ridepal.models.PlaylistDto;
import com.telerikacademy.ridepal.services.GenresService;
import com.telerikacademy.ridepal.services.PlaylistsService;
import com.telerikacademy.ridepal.services.location.BingMapsDistance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Controller
public class HomeController {
    private PlaylistsService playlistsService;
    private GenresService genresService;
    private BingMapsDistance bingMapsDistance;

    @Autowired
    public HomeController(PlaylistsService playlistsService, GenresService genresService, BingMapsDistance bingMapsDistance) {
        this.playlistsService = playlistsService;
        this.genresService = genresService;
        this.bingMapsDistance = bingMapsDistance;
    }

    @GetMapping("/")
    public String homePageGeneratePlaylistForm(Model model) {
        model.addAttribute("playlist", new PlaylistDto());
        return "index";
    }

    @PostMapping("/")
    public String generatePlaylist(@Valid @ModelAttribute("playlist") PlaylistDto playlistDto,
                                   BindingResult errors) {
        if (errors.hasErrors()) {
            return "index";
        }
        StringBuilder startLocation = new StringBuilder();
        StringBuilder endLocation = new StringBuilder();
        startLocation.append(playlistDto.getFromCity()).append(", ").append(playlistDto.getFromCountry()).append(", ").append(playlistDto.getFromAddress());
        endLocation.append(playlistDto.getToCity()).append(", ").append(playlistDto.getToCountry()).append(", ").append(playlistDto.getToAddress());
        try {
            Double time = bingMapsDistance.getCoordinateAddress(startLocation.toString(), endLocation.toString());
            HashMap<Genre, Integer> genreIntegerHashMap = genresHashMap(playlistDto);
            playlistsService.generatePlaylist(playlistDto.getTitle(),
                    time, genreIntegerHashMap,
                    playlistDto.isAllowSameArtists(),
                    playlistDto.isUseTopRankedTracks());
        } catch (WrongArgumentException wae){
            return "index";
        } catch (IOException re){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return "redirect:/playlists";
    }

    @ModelAttribute("playlists")
    public List<Playlist> populatePlaylists() {
        return playlistsService.findTop8Playlists();
    }

    @ModelAttribute("genres")
    public List<Genre> populateGenres() {
        return genresService.findAll();
    }

    @ModelAttribute("activeGenres")
    public List<Genre> populateActiveGenres() {
        return genresService.findActiveGenres();
    }

    @ModelAttribute("lastPlaylist")
    public Playlist populateLastPlaylist() {
        return playlistsService.findTopByOrderByIdDesc();
    }

    @ModelAttribute("topPlaylist")
    public Playlist populateTopPlaylist() { return playlistsService.findTopByOrderByRankDesc();}

    private HashMap<Genre, Integer> genresHashMap(PlaylistDto playlistDto) {
        HashMap<Genre, Integer> genreIntegerHashMap = new HashMap<>();
        for (int i = 0; i < playlistDto.getPercents().size(); i++) {
            if(playlistDto.getPercents().get(i)!=0){
                Genre genre = genresService.findById(playlistDto.getGenres().get(i));
                genreIntegerHashMap.put(genre, playlistDto.getPercents().get(i));
            }
        }
        return genreIntegerHashMap;
    }
}
