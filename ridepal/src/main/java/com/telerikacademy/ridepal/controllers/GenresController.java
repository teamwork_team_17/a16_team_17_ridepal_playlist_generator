package com.telerikacademy.ridepal.controllers;

import com.telerikacademy.ridepal.services.GenresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller

public class GenresController {
    private GenresService genresService;

    @Autowired
    public GenresController(GenresService genresService) {
        this.genresService = genresService;
    }

    @GetMapping("/genres")
    public String showPlaylists(Model model) {
        model.addAttribute("genres", genresService.findAll());
        return "genres";
    }
}
