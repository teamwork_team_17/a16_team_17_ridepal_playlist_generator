package com.telerikacademy.ridepal.controllers;

import com.telerikacademy.ridepal.models.Genre;
import com.telerikacademy.ridepal.models.Playlist;
import com.telerikacademy.ridepal.models.PlaylistDtoEdit;
import com.telerikacademy.ridepal.models.UserInfo;
import com.telerikacademy.ridepal.services.GenresService;
import com.telerikacademy.ridepal.services.PlaylistsService;
import com.telerikacademy.ridepal.services.UsersInfoService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Controller
public class PlaylistsController {
    private PlaylistsService playlistsService;
    private GenresService genresService;
    private UsersInfoService usersInfoService;

    public PlaylistsController(PlaylistsService playlistsService,
                               GenresService genresService,
                               UsersInfoService usersInfoService) {
        this.playlistsService = playlistsService;
        this.genresService = genresService;
        this.usersInfoService = usersInfoService;
    }

    @GetMapping("/playlists")
    public String showPlaylists(@RequestParam(value = "title", required = false, defaultValue = "") String title,
                                @RequestParam(value = "genre", required = false, defaultValue = "") String genre,
                                @RequestParam(value = "duration", required = false, defaultValue = "") String duration,
                                Model model) {
        String from = "0";
        String to = "0";
        if(duration!=null&&!duration.isEmpty()) {
            from = duration.split("-")[0];
            to = duration.split("-")[1];
        }
        model.addAttribute("genres", genresService.findAll());
        model.addAttribute("playlists", playlistsService.findAll(genre,from, to, title));
        return "newPlaylists";
    }

    @GetMapping("/playlists/{id}")
    public String showPlaylistForm(Model model, @PathVariable String id) {
        model.addAttribute("playlist", playlistsService.findPlaylistById(Long.parseLong(id)));
        model.addAttribute("genres", genresService.findAll());
        Playlist playlist = playlistsService.findPlaylistById(Long.parseLong(id));
        PlaylistDtoEdit playlistDtoEdit = new PlaylistDtoEdit();
        playlistDtoEdit.setTitle(playlist.getTitle());
        playlistDtoEdit.setGenreList(getGenresIds(playlist));
        model.addAttribute("playlistDtoEdit", playlistDtoEdit);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo user = usersInfoService.findByUsername(authentication.getName());
        model.addAttribute("user", user);
        return "playlistPage";
    }

    @PostMapping("/playlists/{id}/like")
    public String playlistToFavourites(@PathVariable String id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo user = usersInfoService.findByUsername(authentication.getName());
        user.getPlaylists().add(playlistsService.findPlaylistById(Long.parseLong(id)));
        usersInfoService.updatePlaylists(user);
        return "redirect:/playlists/" + id;
    }

    @PostMapping("/playlists/{id}/unlike")
    public String playlistRemoveFavourites(@PathVariable String id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo user = usersInfoService.findByUsername(authentication.getName());
        user.getPlaylists().remove(playlistsService.findPlaylistById(Long.parseLong(id)));
        usersInfoService.updatePlaylists(user);
        return "redirect:/playlists/" + id;
    }

    @PostMapping("/playlists/{id}")
    public String editPlaylist(@Valid @ModelAttribute PlaylistDtoEdit playlistDtoEdit,BindingResult errors,
                               @PathVariable String id, Model model) {
        if(errors.hasErrors()) {
            model.addAttribute("playlist", playlistsService.findPlaylistById(Long.parseLong(id)));
            model.addAttribute("genres", genresService.findAll());
            model.addAttribute("playlistDtoEdit", playlistDtoEdit);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserInfo user = usersInfoService.findByUsername(authentication.getName());
            model.addAttribute("user", user);
            return "playlistPage";
        }
        Playlist playlist = playlistsService.findPlaylistById(Long.parseLong(id));

        playlistsService.updateTitle(playlistDtoEdit.getTitle(), playlist);
        Set<Genre> genreSet = new HashSet<>();
        for (Integer i: playlistDtoEdit.getGenreList()) {
            Genre genre = genresService.findById(i);
            genreSet.add(genre);
        }
        playlistsService.updateGenres(genreSet, playlist);
        return "redirect:/playlists/" + id;
    }

    @PostMapping("/playlists/{id}/delete")
    public String deletePlaylist(@PathVariable String id) {

        playlistsService.deletePlaylist(Long.parseLong(id));
        return "redirect:/playlists";
    }

    private List<Integer> getGenresIds(Playlist playlist) {
        List<Integer> integerList = new ArrayList<>();
        for (Genre genre : playlist.getGenres()) {
            integerList.add(genre.getId());
        }
        return integerList;
    }

}
