package com.telerikacademy.ridepal.controllers;

import com.telerikacademy.ridepal.exceptions.DuplicateEntityException;
import com.telerikacademy.ridepal.models.User;
import com.telerikacademy.ridepal.services.UsersInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class RegistrationController {
    private UsersInfoService usersInfoService;

    @Autowired
    public RegistrationController(UsersInfoService usersInfoService) {
        this.usersInfoService = usersInfoService;
    }

    @GetMapping("/registration")
    public String showRegistrationPage(Model model) {
        model.addAttribute("user", new User());
        return "registration";
    }

    @PostMapping("/registration")
    public String registerUser(
            @Valid @ModelAttribute("user") User user,
            BindingResult bindingResult,
            Model model) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }

        try {
            usersInfoService.save(user);
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getLocalizedMessage());
            return "registration";
        }

        return "registration-confirmation";
    }

    @GetMapping("/registration-confirmation")
    public String showRegisterConfirmation() {
        return "registration-confirmation";
    }
}
