package com.telerikacademy.ridepal.controllers;

import com.telerikacademy.ridepal.exceptions.DuplicateEntityException;
import com.telerikacademy.ridepal.models.Genre;
import com.telerikacademy.ridepal.models.Playlist;
import com.telerikacademy.ridepal.models.PlaylistDtoEdit;
import com.telerikacademy.ridepal.services.DeezerService;
import com.telerikacademy.ridepal.models.UserInfo;
import com.telerikacademy.ridepal.services.GenresService;
import com.telerikacademy.ridepal.services.PlaylistsService;
import com.telerikacademy.ridepal.services.UsersInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.*;

@Controller
public class AdminController {
    private UsersInfoService usersInfoService;
    private PlaylistsService playlistsService;
    private GenresService genresService;
    private DeezerService deezerService;

    @Autowired
    public AdminController(UsersInfoService usersInfoService, PlaylistsService playlistsService,
                           GenresService genresService, DeezerService deezerService) {
        this.usersInfoService = usersInfoService;
        this.playlistsService = playlistsService;
        this.genresService = genresService;
        this.deezerService = deezerService;
    }

    @GetMapping("/admin")
    public String adminPanel(Model model) {
        model.addAttribute("playlists", playlistsService.findAll());
        model.addAttribute("users", usersInfoService.findAll());
        model.addAttribute("genres", genresService.findAll());
        return "adminPage";
    }

    @GetMapping("/admin/playlists/{id}/edit")
    public String editPlaylistForm(@PathVariable String id, Model model) {
        Playlist playlist = playlistsService.findPlaylistById(Long.parseLong(id));
        model.addAttribute("playlist", playlist);
        model.addAttribute("genres", genresService.findAll());
        PlaylistDtoEdit playlistDtoEdit = new PlaylistDtoEdit();
        playlistDtoEdit.setTitle(playlist.getTitle());
        playlistDtoEdit.setGenreList(getGenresIds(playlist));
        model.addAttribute("playlistDtoEdit", playlistDtoEdit);
        return "playlistEditPage";
    }

    @PostMapping("/admin/playlists/{id}/edit")
    public String editPlaylist(@Valid @ModelAttribute PlaylistDtoEdit playlistDtoEdit,
                               BindingResult errors, @PathVariable String id, Model model) {
        if(errors.hasErrors()) {
            model.addAttribute("genres", genresService.findAll());
            return "playlistEditPage";
        }
        Playlist playlist = playlistsService.findPlaylistById(Long.parseLong(id));
        playlistsService.updateTitle(playlistDtoEdit.getTitle(), playlist);
        Set<Genre> genres = getGenresSet(playlistDtoEdit);
        playlistsService.updateGenres(genres, playlist);
        return "redirect:/playlists/" + id;
    }

    @PostMapping("/admin/users/{id}/delete")
    public String deleteUser(@PathVariable String id) {
        usersInfoService.deleteById(Long.parseLong(id));
        return "redirect:/admin";
    }

    @PostMapping("/admin/playlists/{id}/delete")
    public String deletePlaylist(@PathVariable String id) {
        playlistsService.deletePlaylist(Long.parseLong(id));
        return "redirect:/admin";
    }

    @GetMapping("/admin/users/{id}/edit")
    public String showEditUser(@PathVariable Long id, Model model) {
        UserInfo userInfo = usersInfoService.findUserById(id);
        model.addAttribute("user", userInfo);
        return "editUserPage";
    }

    @PostMapping("/admin/users/{id}/edit")
    public String editUser(@Valid @ModelAttribute("user") UserInfo user,
                           @PathVariable("id") Long id,
                           Model model,
                           Map<String, Object> action) {
        action.put("action", "/admin/users/{id}/edit");
        try {
            usersInfoService.update(user, id);
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getLocalizedMessage());
            return "editUserPage";
        }
        return "redirect:/admin";
    }

    @PostMapping("/admin/sync")
    public String syncGenres(@RequestParam(value = "genreid", required = false, defaultValue = "") String genreid,
                             @RequestParam(value = "songs", required = false, defaultValue = "") String songs) {

        Genre genreToUpdate = genresService.findById(Integer.parseInt(genreid));
        deezerService.loadTracksInDataBase(genreToUpdate, Integer.parseInt(songs));
        return "redirect:/admin";
    }

    private Set<Genre> getGenresSet(PlaylistDtoEdit playlistDtoEdit) {
        Set<Genre> genres = new HashSet<>();
        for (Integer i : playlistDtoEdit.getGenreList()) {
            Genre genre = genresService.findById(i);
            genres.add(genre);
        }
        return genres;
    }

    private List<Integer> getGenresIds(Playlist playlist) {
        List<Integer> integerList = new ArrayList<>();
        for (Genre genre : playlist.getGenres()) {
            integerList.add(genre.getId());
        }
        return integerList;
    }
}
