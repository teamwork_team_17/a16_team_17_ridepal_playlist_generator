package com.telerikacademy.ridepal.controllers;

import com.telerikacademy.ridepal.exceptions.DuplicateEntityException;
import com.telerikacademy.ridepal.models.ChangePassword;
import com.telerikacademy.ridepal.models.Playlist;
import com.telerikacademy.ridepal.models.UserInfo;
import com.telerikacademy.ridepal.services.UsersInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Map;
import java.util.Set;

@Controller
public class UsersController {
    private UsersInfoService usersInfoService;

    @Autowired
    public UsersController(UsersInfoService usersInfoService) {
        this.usersInfoService = usersInfoService;
    }

    @GetMapping("/profile")
    public String showUserPage(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo user = usersInfoService.findByUsername(authentication.getName());

        Set<Playlist> playlistsUserFavorite = usersInfoService.getPlaylistsOfUser(user.getId());
        model.addAttribute("user", user);
        model.addAttribute("playlists", playlistsUserFavorite);

        return "profilePage";
    }

    @GetMapping("/profile/{id}/edit")
    public String showEditUser(@PathVariable Long id, Model model) {
        UserInfo userInfo = usersInfoService.findUserById(id);
        model.addAttribute("user", userInfo);
        return "editUserPage";
    }

    @PostMapping("/profile/{id}/edit")
    public String editUser(@Valid @ModelAttribute("user") UserInfo user,
                           @PathVariable("id") Long id,
                           Model model,
                           Map<String, Object> action) {
        action.put("action", "/admin/users/{id}/edit");
        try {
            usersInfoService.update(user, id);
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getLocalizedMessage());
            return "editUserPage";
        }
        return "redirect:/profile";
    }

    @GetMapping("/profile/{id}/change-password")
    public String showPasswordEditor(@PathVariable Long id, Model model) {
        model.addAttribute("password", new ChangePassword());
        return "editPasswordPage";
    }

    @PostMapping("/profile/{id}/change-password")
    public String passwordEditor(@Valid @ModelAttribute("password") ChangePassword changePassword,
                                 @PathVariable("id") Long id,
                                 HttpServletRequest request) {
        usersInfoService.changePassword(changePassword.getOldPassword(), changePassword.getNewPassword());
        try {
            request.logout();
        } catch (ServletException e) {
            e.printStackTrace();
        }
        return "redirect:/login";
    }

    @PostMapping("/profile/{id}/delete")
    public String deleteAccount(@PathVariable Long id, HttpServletRequest request) {
        try {
            SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);
            SecurityContextHolder.clearContext();
            request.logout();
        } catch (ServletException e) {
            e.printStackTrace();
        }

        usersInfoService.deleteById(id);

        return "delete-confirmation";
    }

    @GetMapping("/delete-confirmation")
    public String showDeleteConfirmation() {
        return "delete-confirmation";
    }
}
