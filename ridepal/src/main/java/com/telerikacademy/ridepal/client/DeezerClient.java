/*
package com.telerikacademy.ridepal.client;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerikacademy.ridepal.models.*;
import com.telerikacademy.ridepal.repositories.GenresRepository;
import com.telerikacademy.ridepal.repositories.TracksRepository;
import com.telerikacademy.ridepal.services.AlbumsService;
import com.telerikacademy.ridepal.services.ArtistsService;
import com.telerikacademy.ridepal.services.PlaylistsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Configuration
public class DeezerClient {
    private GenresRepository genresRepository;
    private PlaylistsService playlistsService;
    private TracksRepository tracksRepository;
    private AlbumsService albumsService;
    private ArtistsService artistsService;

    @Autowired
    public DeezerClient(GenresRepository genresRepository, PlaylistsService playlistsService,
                        TracksRepository tracksRepository,
                        AlbumsService albumsService, ArtistsService artistsService) {
        this.genresRepository = genresRepository;
        this.playlistsService = playlistsService;
        this.tracksRepository = tracksRepository;
        this.albumsService = albumsService;
        this.artistsService = artistsService;
    }

    @Bean
    public CommandLineRunner run() {
        return args -> {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            for (Genre genre : get3genres()) {
                int counter=0;
                JsonNode parent= mapper.readTree(new URL("https://api.deezer.com/search/playlist?q=" + genre.getName() )).path("data");
                List<Playlist> myObjects = Arrays.asList(mapper.readValue(parent.toPrettyString(), Playlist[].class));
                for (Playlist a: myObjects) {
                    if(a.getId() == 1724551385){
                        continue;
                    }
                    JsonNode parentTracks= mapper.readTree(new URL("https://api.deezer.com/playlist/" + a.getId() + "/tracks&limit=60")).path("data");
                    List<Track> tracks = Arrays.asList(mapper.readValue(parentTracks.toPrettyString(), Track[].class));

                    if(tracksRepository.count()>3000){
                        break;
                    }
                    for (Track t: tracks) {
                        if(counter>1000){
                            continue;
                        }
                        Album newAlbum = new Album();
                        newAlbum.setTitle(t.getAlbum().getTitle());
                        newAlbum.setTracklist(t.getAlbum().getTracklist());
                        if(!albumsService.existsByTitle(newAlbum.getTitle())) {
                            albumsService.save(newAlbum);
                        }
                        Artist artist = new Artist();
                        artist.setName(t.getArtist().getName());
                        artist.setTracklist(t.getArtist().getTracklist());
                        if(!artistsService.existsByName(artist.getName())) {
                            artistsService.save(artist);
                        }
                        if(!tracksRepository.existsByTitle(t.getTitle())) {
                            t.setAlbum(albumsService.findByTitle(newAlbum.getTitle()).get(0));
                            t.setArtist(artistsService.findByName(artist.getName()));
                            t.setGenre(genre);
                            tracksRepository.save(t);
                            counter++;
                        }
                    }
                    if(counter>1000){
                        break;
                    }
                }

            }
        };
    }

    private Set<Genre> get3genres() {
        Set<Genre> genres = new HashSet<>();
        genres.add(genresRepository.getByName("Dance"));
        genres.add(genresRepository.getByName("Pop"));
        return genres;
    }
}
*/
