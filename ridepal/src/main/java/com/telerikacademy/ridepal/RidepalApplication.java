package com.telerikacademy.ridepal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RidepalApplication {
    public static void main(String[] args) {
        SpringApplication.run(RidepalApplication.class, args);
    }

}
