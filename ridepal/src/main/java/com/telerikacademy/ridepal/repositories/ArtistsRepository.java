package com.telerikacademy.ridepal.repositories;

import com.telerikacademy.ridepal.models.Artist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArtistsRepository extends JpaRepository<Artist, Long> {
    boolean existsByName(String name);

    Artist findByName(String name);

    Artist findArtistById(Long id);
}
