package com.telerikacademy.ridepal.repositories;

import com.telerikacademy.ridepal.models.Artist;
import com.telerikacademy.ridepal.models.Genre;
import com.telerikacademy.ridepal.models.Track;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TracksRepository extends JpaRepository<Track, Long> {
    boolean existsByTitle(String title);

    List<Track> getByGenre(Genre genre);

    List<Track> getByArtist(Artist artist);

    List<Track> getByGenreOrderByRankDesc(Genre genre);

}
