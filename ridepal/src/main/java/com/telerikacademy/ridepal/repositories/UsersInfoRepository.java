package com.telerikacademy.ridepal.repositories;

import com.telerikacademy.ridepal.models.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersInfoRepository extends JpaRepository<UserInfo, Long> {
    UserInfo findByUsername(String username);

    UserInfo findByEmail(String email);

    UserInfo findUserById(Long id);
}
