package com.telerikacademy.ridepal.repositories;

import com.telerikacademy.ridepal.models.Album;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AlbumsRepository extends JpaRepository<Album, Long> {

    List<Album> findByTitle(String title);

    Album findAlbumById(Long id);

    boolean existsByTitle(String title);
}
