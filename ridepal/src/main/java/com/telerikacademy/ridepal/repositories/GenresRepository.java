package com.telerikacademy.ridepal.repositories;

import com.telerikacademy.ridepal.models.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface GenresRepository extends JpaRepository<Genre, Integer> {
    Genre getByName(String name);

    @Query(value = "select * from genres join tracks t on genres.id = t.genre_id group by genre_id", nativeQuery = true)

    List<Genre> findActiveGenres();

    Genre getById(Integer id);

    boolean existsByName(String name);
}
