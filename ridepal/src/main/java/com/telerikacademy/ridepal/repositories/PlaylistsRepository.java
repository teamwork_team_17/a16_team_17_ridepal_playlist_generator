package com.telerikacademy.ridepal.repositories;

import com.telerikacademy.ridepal.models.Playlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlaylistsRepository extends JpaRepository<Playlist, Long>, JpaSpecificationExecutor<Playlist> {
    Playlist findPlaylistById(Long id);

    Playlist findTopByOrderByIdDesc();

    Playlist findTopByOrderByRankDesc();

    List<Playlist> findTop8ByOrderByRankDesc();
}
