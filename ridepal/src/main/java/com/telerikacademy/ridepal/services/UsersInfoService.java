package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.models.Playlist;
import com.telerikacademy.ridepal.models.User;
import com.telerikacademy.ridepal.models.UserInfo;

import java.util.List;
import java.util.Set;

public interface UsersInfoService {
    UserInfo findByUsername(String username);

    UserInfo findByEmail(String email);

    UserInfo findUserById(Long id);

    List<UserInfo> findAll();

    UserInfo save(User user);

    void updatePlaylists(UserInfo userInfo);

    UserInfo update(UserInfo userToUpdate, Long id);

    void changePassword(String oldPassword, String newPassword);

    void deleteById(Long id);

    Set<Playlist> getPlaylistsOfUser(Long userId);
}
