package com.telerikacademy.ridepal.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.*;
import com.telerikacademy.ridepal.repositories.PlaylistsRepository;
import com.telerikacademy.ridepal.repositories.TracksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.util.*;

@Service
public class PlaylistsServiceImpl implements PlaylistsService{

    private PlaylistsRepository playlistsRepository;
    private TracksRepository tracksRepository;
    private UsersInfoService usersInfoService;

    @Autowired
    public PlaylistsServiceImpl(PlaylistsRepository playlistsRepository,
                                TracksRepository tracksRepository,
                                UsersInfoService usersInfoService) {
        this.playlistsRepository = playlistsRepository;
        this.tracksRepository = tracksRepository;
        this.usersInfoService = usersInfoService;
    }

    @Override
    public List<Playlist> findAll(String ... args) {
        if(args.length==0){
            //for pagination - PageRequest.of(page, 9, Sort.by(Sort.Direction.DESC,"rank"))
            return playlistsRepository.findAll();
        }else {
            List<Playlist> list = playlistsRepository.findAll(
                    PlaylistSpecification.filterByDurationFrom(Integer.parseInt(args[1]))
                            .and(PlaylistSpecification.filterByTitle(args[3]))
                            .and(PlaylistSpecification.filterByDurationTo(Integer.parseInt(args[2])))
                            .and(PlaylistSpecification.getPlaylistByGenres(args[0])), Sort.by(Sort.Direction.DESC,"rank"));
            return list;
        }
    }

    @Override
    public Playlist findPlaylistById(Long id) {
        return playlistsRepository.findPlaylistById(id);
    }


    public void generatePlaylist(String title, Double time, HashMap<Genre, Integer> genres,
                                     boolean allowSameArtists, boolean useTopRankedTracks){
        Playlist playlist = new Playlist();
        int percentage = 0;
        for (Integer integer:genres.values()) {
            percentage = percentage + integer;
        }
        if(percentage!=100){
            throw new IllegalArgumentException("Invalid percents!");
        }
        HashMap<Genre, Integer> genresDurationInSeconds = new HashMap<>();
        for (Genre genre:genres.keySet()) {
            genresDurationInSeconds.put(genre,(genres.get(genre)*(time.intValue()*60))/100);
        }
        playlist.setGenres(genresDurationInSeconds.keySet());

        Integer playtime;
        Integer avgRating = 0;
        Set<Track> tracksForPlaylist = new HashSet<>();
        for (Genre genre: genresDurationInSeconds.keySet()) {
            List<Track> tracks = new ArrayList<>();
            playtime = genresDurationInSeconds.get(genre);
            if(useTopRankedTracks){
                tracks.addAll(tracksRepository.getByGenreOrderByRankDesc(genre));
            }else{
                tracks.addAll(tracksRepository.getByGenre(genre));
            }

            while(playtime>150) {
                if (useTopRankedTracks) {
                    Track topTrack = tracks.get(0);
                    playtime = playtime - topTrack.getDuration();
                    tracksForPlaylist.add(topTrack);
                    avgRating += topTrack.getRank();
                    if (!allowSameArtists) {
                        tracks.removeAll(tracksRepository.getByArtist(topTrack.getArtist()));
                    }else{
                        tracks.remove(0);
                    }
                } else {
                    Random rand = new Random();
                    Track randomTrack = tracks.get(rand.nextInt(tracks.size()));
                    playtime = playtime - randomTrack.getDuration();
                    tracksForPlaylist.add(randomTrack);
                    avgRating += randomTrack.getRank();
                    if (!allowSameArtists) {
                        tracks.removeAll(tracksRepository.getByArtist(randomTrack.getArtist()));
                    }else {
                        tracks.remove(randomTrack);
                    }
                }
            }
        }

        playlist.setPlaytime(tracksForPlaylist.stream().mapToInt(Track::getDuration).sum());
        playlist.setRank((double) (avgRating/tracksForPlaylist.size()));
        playlist.setTitle(title);
        playlist.setTracks(tracksForPlaylist);
        String picture = getImageUrlFromPixabay(genres.keySet().stream().findFirst().get().getName());
        playlist.setPicture(picture);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = usersInfoService.findByUsername(authentication.getName());
        userInfo.getPlaylists().add(playlist);
        playlist.setUser(userInfo);
        playlistsRepository.save(playlist);
    }

    @Override
    public Playlist findTopByOrderByIdDesc() {
        return playlistsRepository.findTopByOrderByIdDesc();
    }

    @Override
    public Playlist findTopByOrderByRankDesc() {
        return playlistsRepository.findTopByOrderByRankDesc();
    }

    @Override
    public void updateTitle(String title, Playlist playlist) {
        if(title==null||title.isEmpty()){
            throw new IllegalArgumentException("Title should not be empty");
        }
        playlist.setTitle(title);
        playlistsRepository.save(playlist);
    }

    @Override
    public void updateGenres(Set<Genre> genres, Playlist playlist) {
        if(genres==null||genres.isEmpty()){
            throw new IllegalArgumentException("Add at least one genre");
        }
        playlist.setGenres(genres);
        playlistsRepository.save(playlist);
    }

    @Override
    public void deletePlaylist(Long id) {
        if(!playlistsRepository.existsById(id)){
            throw new EntityNotFoundException("Playlist",id);
        }
        Playlist playlist = playlistsRepository.findPlaylistById(id);
        List<UserInfo> userInfoList = usersInfoService.findAll();
        for (UserInfo user: userInfoList) {
            user.getPlaylists().remove(playlist);
            usersInfoService.updatePlaylists(user);
        }

        playlistsRepository.deleteById(id);
    }

    @Override
    public List<Playlist> findTop8Playlists() {
        return playlistsRepository.findTop8ByOrderByRankDesc();
    }

    public String getImageUrlFromPixabay(String genre) {
        String url = "https://pixabay.com/api/?key=15124320-7990f63490a736691fa9d8def&q=music+" + genre + "&image_type=photo&pretty=true&per_page=100";
        String result = "";
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        try {
            JsonNode parent = mapper.readTree(new URL(url)).path("hits");
            List<String> urls = Arrays.asList(mapper.readValue(parent.findValues("webformatURL").toString(), String[].class));
            Collections.shuffle(urls);
            result = urls.get(0);
        }catch (IOException e){
            throw new IllegalArgumentException();
        }
        return result;
    }
}
