package com.telerikacademy.ridepal.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerikacademy.ridepal.exceptions.DuplicateEntityException;
import com.telerikacademy.ridepal.models.Genre;
import com.telerikacademy.ridepal.repositories.GenresRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;


@Service
public class GenresServiceImpl implements GenresService {
    private GenresRepository genresRepository;

    @Autowired
    public GenresServiceImpl(GenresRepository genresRepository) {
        this.genresRepository = genresRepository;
    }


    @Override
    public void generateGenres() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        try {
            JsonNode parent = mapper.readTree(new URL("https://api.deezer.com/genre")).path("data");
            List<Genre> genres = Arrays.asList(mapper.readValue(parent.toPrettyString(), Genre[].class));
            for (Genre genre : genres) {
                if (genresRepository.existsByName(genre.getName())) {
                    throw new DuplicateEntityException(String.format("Genre with name %s already exists.", genre.getName()));
                }
                if (genre.getName().equals("All")) {
                    continue;
                } else {
                    genresRepository.save(genre);
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public List<Genre> findAll() {
        return genresRepository.findAll();
    }

    @Override
    public List<Genre> findActiveGenres() {
        return genresRepository.findActiveGenres();
    }

    public Genre findById(Integer id) {
        return genresRepository.getById(id);
    }
}
