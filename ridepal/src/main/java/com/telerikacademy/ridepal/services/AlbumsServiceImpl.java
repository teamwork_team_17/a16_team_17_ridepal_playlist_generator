package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.exceptions.DuplicateEntityException;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Album;
import com.telerikacademy.ridepal.repositories.AlbumsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlbumsServiceImpl implements AlbumsService {
    private AlbumsRepository albumsRepository;

    @Autowired
    public AlbumsServiceImpl(AlbumsRepository albumsRepository) {
        this.albumsRepository = albumsRepository;
    }

    @Override
    public List<Album> findByTitle(String title) {
        return albumsRepository.findByTitle(title);
    }

    @Override
    public Album findAlbumById(Long id) {
        return albumsRepository.findAlbumById(id);
    }

    @Override
    public List<Album> findAll() {
        return albumsRepository.findAll();
    }

    @Override
    public Album save(Album album) {
        if (!findByTitle(album.getTitle()).isEmpty()) {
            throw new DuplicateEntityException(String.format("Album with name %s already exists.", album.getTitle()));
        }
        return albumsRepository.save(album);
    }

    @Override
    public Album update(Album albumToUpdate, Long id) {
        if (!albumsRepository.existsById(id)) {
            throw new EntityNotFoundException("Album", id);
        }
        if (!findByTitle(albumToUpdate.getTitle()).isEmpty()) {
            throw new DuplicateEntityException(String.format("Album with name %s already exists.", albumToUpdate.getTitle()));
        }
        Album album = albumsRepository.findAlbumById(id);
        album.setTitle(albumToUpdate.getTitle());
        album.setTracklist(albumToUpdate.getTracklist());
        return albumsRepository.save(album);
    }

    @Override
    public void deleteById(Long id) {
        if (!albumsRepository.existsById(id)) {
            throw new EntityNotFoundException("Album", id);
        }
        albumsRepository.deleteById(id);
    }

    @Override
    public boolean existsByTitle(String title) {
        return albumsRepository.existsByTitle(title);
    }
}
