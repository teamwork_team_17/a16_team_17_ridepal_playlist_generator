package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.models.Artist;

import java.util.List;

public interface ArtistsService {
    Artist findByName(String name);

    Artist findArtistById(Long id);

    List<Artist> findAll();

    Artist save(Artist artist);

    Artist update(Artist artist, Long id);

    void deleteById(Long id);

    boolean existsByName(String name);
}
