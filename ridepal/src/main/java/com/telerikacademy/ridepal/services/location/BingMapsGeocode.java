package com.telerikacademy.ridepal.services.location;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class BingMapsGeocode {
    private static final String EQUAL = "=";
    private static final String AND = "&";
    private static final String PARAM_KEY = "key";
    private static final String PARAM_OUTPUT = "output";
    private static final String PARAM_OUTPUT_TYPE = "json";
    private static final String PARAM_COUNTRY = "countryRegion";
    private static final String PARAM_CITY = "locality";
    private static final String PARAM_ADDRESS = "addressLine";

    public String getCoordinateAddress(String city, String country, String streetAddress) {
        String dynamicURl = prepareDynamicURL(city, country, streetAddress);

        String result = null;
        if (!StringUtils.isEmpty(dynamicURl)) {
            result = getResultHttp(dynamicURl);
        }

        return result;
    }

    private String prepareDynamicURL(String city, String country, String streetAddress) {
        StringBuilder sb = new StringBuilder();
        String bingLocationUrl = "http://dev.virtualearth.net/REST/v1/Locations";
        String bingAuthenticationKey = "xjJJGdf0KJtqxZte3EsH~bNI_xVJLcRhnkZh7tljVbw~Ahn6Vgb9vWl-lPHQ4G0ttAVrUAktgSOeWtSAgKPIPQtaMB1HQHkZ3hfFuYXcB9Ba";

        sb.append(bingLocationUrl).append("?");
        sb.append(PARAM_COUNTRY).append(EQUAL).append(country).append(AND);
        try {
            sb.append(PARAM_CITY).append(EQUAL).append(encodeString(city)).append(AND);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (!streetAddress.equals("-")) {
            try {
                sb.append(PARAM_ADDRESS).append(EQUAL).append(encodeString(streetAddress)).append(AND);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        sb.append(PARAM_KEY).append(EQUAL).append(bingAuthenticationKey).append(AND);

        sb.append(PARAM_OUTPUT).append(EQUAL).append(PARAM_OUTPUT_TYPE);

        return sb.toString();
    }

    private String encodeString(String str)
            throws UnsupportedEncodingException {
        return URLEncoder.encode(str.replace(".", "").replace(",", "").replace(":", ""), "UTF-8");
    }

    private String getResultHttp(String url) {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url)).build();
        return client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .thenApply(this::getGeocode)
                .join();
    }

    private String getGeocode(String responseBody) {
        List<Double> geocodePoints = new ArrayList<>();
        JSONObject response = new JSONObject(responseBody);

        JSONObject resourceSets = response.getJSONArray("resourceSets").getJSONObject(0);
        JSONObject resources = resourceSets.getJSONArray("resources").getJSONObject(0);

        JSONArray geocode = resources.getJSONArray("geocodePoints");
        if (geocode.length() > 1) {
            for (int i = 0; i < geocode.length(); i++) {
                JSONObject points = geocode.getJSONObject(i);
                JSONArray usageTypes = points.getJSONArray("usageTypes");
                for (Object usageType : usageTypes) {
                    if (usageType.equals("Route")) {
                        getCoordinates(geocodePoints, points);
                    }
                }
            }
        } else {
            JSONObject points = geocode.getJSONObject(0);
            getCoordinates(geocodePoints, points);
        }

        return geocodePoints.toString();
    }

    private void getCoordinates(List<Double> geocodePoints, JSONObject points) throws JSONException {
        JSONArray geoPoints = points.getJSONArray("coordinates");
        for (int j = 0; j < geoPoints.length(); j++) {
            double point = geoPoints.getDouble(j);
            geocodePoints.add(point);
        }
    }
}