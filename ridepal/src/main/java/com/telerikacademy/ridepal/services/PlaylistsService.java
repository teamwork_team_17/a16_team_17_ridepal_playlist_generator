package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.models.Genre;
import com.telerikacademy.ridepal.models.Playlist;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

public interface PlaylistsService {
    List<Playlist> findAll(String ... args);

    Playlist findPlaylistById(Long id);

    void generatePlaylist(String title, Double time, HashMap<Genre, Integer> genres,
                              boolean allowSameArtists, boolean useTopRankedTracks);

    Playlist findTopByOrderByIdDesc();

    Playlist findTopByOrderByRankDesc();

    void updateTitle(String title, Playlist playlist);

    void updateGenres(Set<Genre> genres, Playlist playlist);

    void deletePlaylist(Long id);

    List<Playlist> findTop8Playlists();
}
