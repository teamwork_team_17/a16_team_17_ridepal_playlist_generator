package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.exceptions.DuplicateEntityException;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Artist;
import com.telerikacademy.ridepal.repositories.ArtistsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArtistsServiceImpl implements ArtistsService {
    private ArtistsRepository artistsRepository;

    @Autowired
    public ArtistsServiceImpl(ArtistsRepository artistsRepository) {
        this.artistsRepository = artistsRepository;
    }

    @Override
    public Artist findByName(String name) {
        return artistsRepository.findByName(name);
    }

    @Override
    public Artist findArtistById(Long id) {
        return artistsRepository.findArtistById(id);
    }

    @Override
    public List<Artist> findAll() {
        return artistsRepository.findAll();
    }

    @Override
    public Artist save(Artist artist) {
        if (existsByName(artist.getName())) {
            throw new DuplicateEntityException(String.format("Artist with name %s already exists.", artist.getName()));
        }
        return artistsRepository.save(artist);
    }

    @Override
    public Artist update(Artist artist, Long id) {
        if (!artistsRepository.existsById(id)) {
            throw new EntityNotFoundException("Artist", id);
        }
        if (existsByName(artist.getName())) {
            throw new DuplicateEntityException(String.format("Artist with name %s already exists.", artist.getName()));
        }
        Artist artist1 = artistsRepository.findArtistById(id);
        artist1.setName(artist.getName());
        artist1.setTracklist(artist.getTracklist());
        return artistsRepository.save(artist1);
    }

    @Override
    public void deleteById(Long id) {
        if (!artistsRepository.existsById(id)) {
            throw new EntityNotFoundException("Artist", id);
        }
        artistsRepository.deleteById(id);
    }

    @Override
    public boolean existsByName(String name) {
        return artistsRepository.existsByName(name);
    }
}
