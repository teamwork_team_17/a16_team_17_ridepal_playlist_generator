package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.models.Genre;

import java.io.IOException;
import java.util.List;

public interface GenresService {
    void generateGenres() throws IOException;

    List<Genre> findAll();

    List<Genre> findActiveGenres();

    Genre findById(Integer id);
}
