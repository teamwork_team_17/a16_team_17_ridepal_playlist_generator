package com.telerikacademy.ridepal.services.location;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Configuration
public class BingMapsDistance {
    private BingMapsGeocode bingMapsGeocode;

    @Autowired
    public BingMapsDistance(BingMapsGeocode bingMapsGeocode) {
        this.bingMapsGeocode = bingMapsGeocode;
    }

    public Double getCoordinateAddress(String startLocation, String finalLocation) throws IOException {
        String[] start = startLocation.split(" ");
        String[] end = finalLocation.split(" ");
        String from = bingMapsGeocode.getCoordinateAddress(start[0], start[1], start[2]);
        String to = bingMapsGeocode.getCoordinateAddress(end[0], end[1], end[2]);
        from = from.substring(1, from.length() - 1).replaceAll("\\s", "");
        to = to.substring(1, to.length() - 1).replaceAll("\\s", "");
        String key = "xjJJGdf0KJtqxZte3EsH~bNI_xVJLcRhnkZh7tljVbw~Ahn6Vgb9vWl-lPHQ4G0ttAVrUAktgSOeWtSAgKPIPQtaMB1HQHkZ3hfFuYXcB9Ba";
        URL url = new URL("https://dev.virtualearth.net/REST/v1/Routes/DistanceMatrix?origins=" + from + "&destinations=" + to + "&travelMode=driving&key=" + key);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        String line;
        StringBuilder outputString = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        while ((line = reader.readLine()) != null) {
            outputString.append(line);
        }

        return getTime(outputString.toString());
    }

    private Double getTime(String responseBody) {
        JSONObject response = new JSONObject(responseBody);

        JSONObject resourceSets = response.getJSONArray("resourceSets").getJSONObject(0);
        JSONObject resources = resourceSets.getJSONArray("resources").getJSONObject(0);
        JSONObject results = resources.getJSONArray("results").getJSONObject(0);

        return results.getDouble("travelDuration");
    }
}