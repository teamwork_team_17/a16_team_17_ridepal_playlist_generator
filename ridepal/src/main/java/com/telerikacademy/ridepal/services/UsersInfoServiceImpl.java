package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.exceptions.DuplicateEntityException;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Playlist;
import com.telerikacademy.ridepal.models.User;
import com.telerikacademy.ridepal.models.UserInfo;
import com.telerikacademy.ridepal.repositories.UsersInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class UsersInfoServiceImpl implements UsersInfoService {
    private UsersInfoRepository usersInfoRepository;
    private PasswordEncoder passwordEncoder;
    private UserDetailsManager userDetailsManager;
    private PlaylistsService playlistsService;

    @Autowired
    public UsersInfoServiceImpl(UsersInfoRepository usersInfoRepository,
                                PasswordEncoder passwordEncoder,
                                UserDetailsManager userDetailsManager,
                                @Lazy PlaylistsService playlistsService) {
        this.usersInfoRepository = usersInfoRepository;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsManager = userDetailsManager;
        this.playlistsService = playlistsService;
    }

    @Override
    public UserInfo findByUsername(String username) {
        return usersInfoRepository.findByUsername(username);
    }

    @Override
    public UserInfo findByEmail(String email) {
        return usersInfoRepository.findByEmail(email);
    }

    @Override
    public UserInfo findUserById(Long id) {
        return usersInfoRepository.findUserById(id);
    }

    @Override
    public List<UserInfo> findAll() {
        return usersInfoRepository.findAll();
    }

    @Override
    public UserInfo save(User user) {
        if (userDetailsManager.userExists(user.getUsername())) {
            throw new DuplicateEntityException("User with the same username already exists!");
        }

        if (findByEmail(user.getEmail()) != null) {
            throw new DuplicateEntityException("User with the same email already exists.");
        }

        UserInfo newUser = new UserInfo();

        newUser.setUsername(user.getUsername());
        newUser.setFirstName(user.getFirstName());
        newUser.setLastName(user.getLastName());
        newUser.setEmail(user.getEmail());

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUserDetails =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        passwordEncoder.encode(user.getPassword()),
                        authorities);
        userDetailsManager.createUser(newUserDetails);
        return usersInfoRepository.save(newUser);
    }

    @Override
    public void updatePlaylists(UserInfo userInfo) {
        usersInfoRepository.save(userInfo);
    }

    @Override
    public UserInfo update(UserInfo userToUpdate, Long id) {
        if (userDetailsManager.userExists(userToUpdate.getUsername())) {
            throw new EntityNotFoundException(String.format("User with id=%d doesn't exists!", id));
        }
        UserInfo userInfo = usersInfoRepository.findUserById(id);
        if (findByEmail(userToUpdate.getEmail()) != null && !userInfo.getEmail().equals(userToUpdate.getEmail())) {
            throw new DuplicateEntityException("User with the same email already exists.");
        }

        userInfo.setFirstName(userToUpdate.getFirstName());
        userInfo.setLastName(userToUpdate.getLastName());
        userInfo.setEmail(userToUpdate.getEmail());

        return usersInfoRepository.save(userInfo);
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
        userDetailsManager.changePassword(oldPassword, passwordEncoder.encode(newPassword));
    }

    @Override
    public void deleteById(Long id) {
        if (!usersInfoRepository.existsById(id)) {
            throw new EntityNotFoundException("User", id);
        }

        UserInfo userInfo = usersInfoRepository.findUserById(id);
        String username = userInfo.getUsername();

        Set<Playlist> playlists = getPlaylistsOfUser(id);
        if (playlists.size() != 0) {
            for (Playlist playlist : playlists) {
                playlistsService.deletePlaylist(playlist.getId());
            }
        }
        usersInfoRepository.deleteById(id);
        userDetailsManager.deleteUser(username);
    }

    @Override
    public Set<Playlist> getPlaylistsOfUser(Long userId) {
        UserInfo userInfo = usersInfoRepository.findUserById(userId);

        return userInfo.getPlaylists();
    }
}
