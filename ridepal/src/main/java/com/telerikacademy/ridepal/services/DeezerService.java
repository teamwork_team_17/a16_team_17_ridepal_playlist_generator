package com.telerikacademy.ridepal.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerikacademy.ridepal.models.*;
import com.telerikacademy.ridepal.repositories.TracksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

@Configuration
public class DeezerService {
    private AlbumsService albumsService;
    private ArtistsService artistsService;
    private TracksRepository tracksRepository;

    @Autowired
    public DeezerService(AlbumsService albumsService, ArtistsService artistsService, TracksRepository tracksRepository) {
        this.albumsService = albumsService;
        this.artistsService = artistsService;
        this.tracksRepository = tracksRepository;
    }

    public void loadTracksInDataBase(Genre genre, Integer numberTracks) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        int counter = 0;
        try {
            JsonNode parent = mapper.readTree(new URL("https://api.deezer.com/search/playlist?q=" + genre.getName())).path("data");
            List<Playlist> myObjects = Arrays.asList(mapper.readValue(parent.toPrettyString(), Playlist[].class));
            for (Playlist a : myObjects) {
                JsonNode parentTracks = mapper.readTree(new URL("https://api.deezer.com/playlist/" + a.getId() + "/tracks&limit=60")).path("data");
                List<Track> tracks = Arrays.asList(mapper.readValue(parentTracks.toPrettyString(), Track[].class));
                for (Track t : tracks) {
                    if (counter > numberTracks) {
                        continue;
                    }
                    Album newAlbum = new Album();
                    newAlbum.setTitle(t.getAlbum().getTitle());
                    newAlbum.setTracklist(t.getAlbum().getTracklist());
                    if (!albumsService.existsByTitle(newAlbum.getTitle())) {
                        albumsService.save(newAlbum);
                    }
                    Artist artist = new Artist();
                    artist.setName(t.getArtist().getName());
                    artist.setTracklist(t.getArtist().getTracklist());
                    if (!artistsService.existsByName(artist.getName())) {
                        artistsService.save(artist);
                    }
                    if (!tracksRepository.existsByTitle(t.getTitle())) {
                        t.setAlbum(albumsService.findByTitle(newAlbum.getTitle()).get(0));
                        t.setArtist(artistsService.findByName(artist.getName()));
                        t.setGenre(genre);
                        tracksRepository.save(t);
                        counter++;
                    }
                }
            }
        }catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }
}
