package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.models.Album;

import java.util.List;

public interface AlbumsService {
    List<Album> findByTitle(String title);

    Album findAlbumById(Long id);

    List<Album> findAll();

    Album save(Album album);

    Album update(Album album, Long id);

    void deleteById(Long id);

    boolean existsByTitle(String title);

}
