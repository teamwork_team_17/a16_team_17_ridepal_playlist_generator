package com.telerikacademy.ridepal;

import com.sun.tools.javac.jvm.Gen;
import com.telerikacademy.ridepal.models.*;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Factory {
    public static UserInfo createUserInfo() {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(1L);
        userInfo.setUsername("testUsername");
        userInfo.setEmail("testEmail@test.com");
        userInfo.setFirstName("testFirstName");
        userInfo.setLastName("testLastName");
        userInfo.setPlaylists(Collections.singleton(createPlaylist()));
        return userInfo;
    }

    public static User createUser() {
        User user = new User();
        user.setUsername("testUsername");
        user.setPassword("testPassword");
        user.setConfirmPassword("testPassword");
        user.setEmail("testEmail@test.com");
        user.setFirstName("testFirstName");
        user.setLastName("testLastName");
        return user;
    }

    public static Playlist createPlaylist() {
        Playlist playlist = new Playlist();
        playlist.setId(1L);
        playlist.setPicture("testPicture");
        playlist.setPlaytime(123);
        playlist.setRank(1.23);
        playlist.setTitle("testTitle");
        Set<Genre> genres = new HashSet<>();
        genres.add(createGenre());
        playlist.setGenres(genres);
        return playlist;
    }

    public static Genre createGenre() {
        Genre genre = new Genre();
        genre.setId(1);
        genre.setName("testGenre");
        genre.setPicture("testPicture");
        return genre;
    }

    public static Track createTrack() {
        Track track = new Track();
        track.setId(1L);
        track.setDuration(123);
        track.setLink("testLink");
        track.setPreview("testPreview");
        track.setRank(123);
        track.setTitle("testTitle");
        return track;
    }

    public static Artist createArtist() {
        Artist artist = new Artist();
        artist.setId(1L);
        artist.setName("testName");
        artist.setTracklist("testTrackList");
        return artist;
    }

    public static Album createAlbum() {
        Album album = new Album();
        album.setId(1L);
        album.setTitle("testTitle");
        album.setTracklist("testTrackList");
        return album;
    }

}
