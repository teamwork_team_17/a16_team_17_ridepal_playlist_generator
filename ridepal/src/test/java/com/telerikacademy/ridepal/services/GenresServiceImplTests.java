package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.exceptions.DuplicateEntityException;
import com.telerikacademy.ridepal.models.Genre;
import com.telerikacademy.ridepal.repositories.GenresRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static com.telerikacademy.ridepal.Factory.createGenre;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class GenresServiceImplTests {
    @Mock
    GenresRepository genresRepository;

    @InjectMocks
    GenresServiceImpl mockGenresService;

    @Test
    public void findActiveGenresShould_ReturnedAllActiveGenres() {
        // Arrange
        Genre expected = createGenre();
        Mockito.when(genresRepository.findActiveGenres())
                .thenReturn(Collections.singletonList(expected));
        // Act
        List<Genre> returned = mockGenresService.findActiveGenres();
        // Assert
        Assert.assertSame(1, returned.size());
    }

    @Test
    public void finArtistByIdShould_ReturnedGenreWithSameId() {
        // Arrange
        Genre expected = createGenre();
        Mockito.when(genresRepository.getById(1))
                .thenReturn(expected);
        // Act
        Genre returned = mockGenresService.findById(1);
        // Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void findAllShould_ReturnedAllGenres() {
        // Arrange
        Genre expected = createGenre();
        Mockito.when(genresRepository.findAll())
                .thenReturn(Collections.singletonList(expected));
        // Act
        List<Genre> returned = mockGenresService.findAll();
        // Assert
        Assert.assertSame(1, returned.size());
    }

    @Test
    public void generateGenresShould_ThrowException_WhenGenreWithNameAlreadyExists() {
        // Arrange
        Mockito.when(genresRepository.existsByName(anyString()))
                .thenReturn(true);
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockGenresService.generateGenres());
    }

    @Test
    public void generateGenresShould_SaveNewGenre() throws IOException {
        // Arrange
        Mockito.when(genresRepository.existsByName(anyString()))
                .thenReturn(false);
        // Act
        mockGenresService.generateGenres();
        // Assert
        Mockito.verify(genresRepository, atLeastOnce())
                .save(any());
    }
}
