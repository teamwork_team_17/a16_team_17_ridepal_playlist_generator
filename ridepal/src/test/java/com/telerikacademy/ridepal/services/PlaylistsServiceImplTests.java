package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.*;
import com.telerikacademy.ridepal.repositories.PlaylistsRepository;
import com.telerikacademy.ridepal.repositories.TracksRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static com.telerikacademy.ridepal.Factory.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class PlaylistsServiceImplTests {
    @Mock
    PlaylistsRepository playlistsRepository;

    @Mock
    TracksRepository tracksRepository;

    @Mock
    UsersInfoService usersInfoService;

    @InjectMocks
    PlaylistsServiceImpl playlistsService;

    @Test
    public void findAllShould_ReturnAllPlaylists() {
        // Arrange
        Playlist expected = createPlaylist();
        Mockito.when(playlistsRepository.findAll())
                .thenReturn(Collections.singletonList(expected));
        // Act
        List<Playlist> returned = playlistsService.findAll();
        // Assert
        Assert.assertSame(1, returned.size());
    }

    /*@Test
    public void findAllShould_ReturnAllPlaylistsWithArguments() {
        // Arrange
        Playlist expected = createPlaylist();
        Mockito.when(playlistsRepository.findAll(PlaylistSpecification.filterByTitle("testTitle"),Sort.by(Sort.Direction.DESC,"rank")))
                .thenReturn(Collections.singletonList(expected));
        // Act
        List<Playlist> returned = playlistsService.findAll("","0","0","testTitle");
        // Assert

        Assert.assertSame(1, returned.size());
    }*/

    @Test
    public void findPlaylistByIdShould_ReturnPlaylist() {
        // Arrange
        Playlist expected = createPlaylist();
        Mockito.when(playlistsRepository.findPlaylistById(1L))
                .thenReturn(expected);
        // Act
        Playlist playlist = playlistsService.findPlaylistById(1L);
        // Assert
        Assert.assertSame(expected, playlist);
    }

    @Test
    public void generatePlaylistShould_ThrowExceptionWhenPercentsNot100() {
        // Arrange
        Genre genre = createGenre();
        HashMap<Genre,Integer> genreIntegerHashMap = new HashMap<>();
        genreIntegerHashMap.put(genre, 99);
        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> playlistsService.generatePlaylist("title",100D,genreIntegerHashMap,true,true));
    }

   /* @Test
    public void generatePlaylistShould_GeneratePlaylist() {
        Playlist expected  = createPlaylist();
        Genre genre = createGenre();
        genre.setName("Pop");
        Set<Genre> genres = new HashSet<>();
        Set<Track> tracks = new HashSet<>();
        Track track = createTrack();
        track.setDuration(180);
        tracks.add(track);
        genres.add(genre);
        expected.setGenres(genres);
        expected.setTracks(tracks);
        HashMap<Genre, Integer> genreIntegerMap = new HashMap<>();
        genreIntegerMap.put(genre, 100);
        UserInfo userInfo = createUserInfo();
        Mockito.when(tracksRepository.getByGenreOrderByRankDesc(genre)).thenReturn(Collections.singletonList(track));
        Mockito.when(usersInfoService.findByUsername("testUsername")).thenReturn(userInfo);
        playlistsService.generatePlaylist("title",3.0,genreIntegerMap,true,true);

        Assert.assertSame(1, playlistsRepository.findAll().size());
    }*/

    @Test
    public void findTopByOrderByIdDescShould_ReturnTopByIdPlaylist() {
        Playlist expected = createPlaylist();
        Mockito.when(playlistsRepository.findTopByOrderByIdDesc()).thenReturn(expected);
        Playlist playlist = playlistsService.findTopByOrderByIdDesc();
        Assert.assertSame(expected, playlist);
    }

    @Test
    public void findTopByOrderByRankDescShould_ReturnTopByRank() {
        Playlist expected = createPlaylist();
        Mockito.when(playlistsRepository.findTopByOrderByRankDesc()).thenReturn(expected);
        Playlist playlist = playlistsService.findTopByOrderByRankDesc();
        Assert.assertSame(expected, playlist);
    }

    @Test
    public void updateTitleShould_ThrowExceptionWhenTitleIsNull() {
        // Arrange
        Playlist expected = createPlaylist();
        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> playlistsService.updateTitle(null, expected));
    }

    @Test
    public void updateTitleShould_ThrowExceptionWhenTitleIsEmpty() {
        // Arrange
        Playlist expected = createPlaylist();
        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> playlistsService.updateTitle("", expected));
    }

    @Test
    public void updateTitleShould_UpdateTitle() {
        // Arrange
        Playlist expected = createPlaylist();
        // Act, Assert
        playlistsService.updateTitle("new", expected);
        Mockito.verify(playlistsRepository,
                times(1)).save(expected);
    }

    @Test
    public void updateGenresShould_ThrowExceptionWhenSetIsEmpty() {
        Playlist expected = createPlaylist();
        Set<Genre> genres = new HashSet<>();
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> playlistsService.updateGenres(genres, expected));
    }

    @Test
    public void updateGenresShould_UpdateGenres() {
        Playlist expected = createPlaylist();
        Set<Genre> genres = new HashSet<>();
        Genre genre = createGenre();
        genres.add(genre);
        playlistsService.updateGenres(genres, expected);
        Mockito.verify(playlistsRepository,
                times(1)).save(expected);
    }

    @Test
    public void deletePlaylistShould_ThrowExceptionWhenPlaylistNotExist() {
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> playlistsService.deletePlaylist(1L));
    }

    @Test
    public void deletePlaylistShould_DeletePlaylist() {
        Playlist playlist = createPlaylist();
        UserInfo userInfo = createUserInfo();
        Set<Playlist> playlists = new HashSet<>();
        playlists.add(playlist);
        userInfo.setPlaylists(playlists);
        Mockito.when(playlistsRepository.existsById(1L)).thenReturn(true);
        Mockito.when(playlistsRepository.findPlaylistById(1L)).thenReturn(playlist);
        Mockito.when(usersInfoService.findAll()).thenReturn(Collections.singletonList(userInfo));
        playlistsService.deletePlaylist(1L);
        Mockito.verify(playlistsRepository, times(1))
                .deleteById(1L);
    }

    @Test
    public void findTop8PlaylistsShould_GetTop8Playlists() {
        Playlist expected = createPlaylist();
        Mockito.when(playlistsRepository.findTop8ByOrderByRankDesc()).thenReturn(Collections.singletonList(expected));
        List<Playlist> returned = playlistsService.findTop8Playlists();
        Assert.assertSame(1, returned.size());
    }
}
