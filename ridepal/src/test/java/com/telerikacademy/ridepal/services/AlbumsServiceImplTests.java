package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.exceptions.DuplicateEntityException;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Album;
import com.telerikacademy.ridepal.repositories.AlbumsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static com.telerikacademy.ridepal.Factory.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class AlbumsServiceImplTests {
    @Mock
    AlbumsRepository albumsRepository;

    @InjectMocks
    AlbumsServiceImpl mockAlbumsService;

    @Test
    public void findByTitleShould_ReturnedAlbumWithSameTitle() {
        // Arrange
        Album expected = createAlbum();
        Mockito.when(albumsRepository.findByTitle("testTitle"))
                .thenReturn(Collections.singletonList(expected));
        // Act
        List<Album> returned = mockAlbumsService.findByTitle("testTitle");
        // Assert
        Assert.assertSame(1, returned.size());
    }

    @Test
    public void findAlbumByIdShould_ReturnedAlbumWithSameId() {
        // Arrange
        Album expected = createAlbum();
        Mockito.when(albumsRepository.findAlbumById(1L))
                .thenReturn(expected);
        // Act
        Album returned = mockAlbumsService.findAlbumById(1L);
        // Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void findAllShould_ReturnedAllAlbums() {
        // Arrange
        Album expected = createAlbum();
        Mockito.when(albumsRepository.findAll())
                .thenReturn(Collections.singletonList(expected));
        // Act
        List<Album> returned = mockAlbumsService.findAll();
        // Assert
        Assert.assertSame(1, returned.size());
    }

    @Test
    public void saveShould_SaveAlbum_WhenAlbumNotExists() {
        // Arrange
        Album expected = createAlbum();
        // Act
        mockAlbumsService.save(expected);
        // Assert
        Mockito.verify(albumsRepository,
                times(1)).save(expected);
    }

    @Test
    public void saveShould_ThrowException_WhenAlbumExists() {
        // Arrange
        Album expected = createAlbum();
        Mockito.when(albumsRepository.findByTitle(expected.getTitle()))
                .thenReturn(Collections.singletonList(createAlbum()));
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockAlbumsService.save(expected));
    }

    @Test
    public void updateShould_UpdateAlbum_WhenAlbumExists() {
        // Arrange
        Album expected = createAlbum();
        Mockito.when(albumsRepository.existsById(1L))
                .thenReturn(true);
        Mockito.when(albumsRepository.findAlbumById(1L))
                .thenReturn(expected);
        // Act
        mockAlbumsService.update(expected, 1L);
        // Assert
        Mockito.verify(albumsRepository, times(1))
                .save(expected);
    }

    @Test
    public void updateShould_ThrowException_WhenAlbumNotExists() {
        // Arrange
        Album expected = createAlbum();
        Mockito.when(albumsRepository.existsById(1L))
                .thenReturn(false);
        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockAlbumsService.update(expected, 1L));
    }

    @Test
    public void updateShould_ThrowException_WhenAlbumWithSameTitleAlreadyExists() {
        // Arrange
        Album expected = createAlbum();
        Mockito.when(albumsRepository.existsById(1L))
                .thenReturn(true);
        Mockito.when(albumsRepository.findByTitle(expected.getTitle()))
                .thenReturn(Collections.singletonList(createAlbum()));
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockAlbumsService.update(expected, 1L));
    }

    @Test
    public void deleteByIdShould_DeleteAlbum_WhenAlbumExists() {
        // Arrange
        Mockito.when(albumsRepository.existsById(1L))
                .thenReturn(true);
        // Act
        mockAlbumsService.deleteById(1L);
        // Assert
        Mockito.verify(albumsRepository, times(1))
                .deleteById(1L);
    }

    @Test
    public void deleteByIdShould_ThrowException_WhenAlbumNotExists() {
        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockAlbumsService.deleteById(1L));
    }

    @Test
    public void existsByTitleShould_CallRepository() {
        // Arrange
        Mockito.when(albumsRepository.existsByTitle(anyString()))
                .thenReturn(true);
        // Act
        mockAlbumsService.existsByTitle(anyString());
        // Assert
        Mockito.verify(albumsRepository, times(1))
                .existsByTitle(anyString());
    }
}
