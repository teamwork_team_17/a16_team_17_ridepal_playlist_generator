package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.exceptions.DuplicateEntityException;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Playlist;
import com.telerikacademy.ridepal.models.User;
import com.telerikacademy.ridepal.models.UserInfo;
import com.telerikacademy.ridepal.repositories.UsersInfoRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.telerikacademy.ridepal.Factory.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class UsersServicesImplTests {
    @Mock
    UsersInfoRepository usersInfoRepository;

    @Mock
    PasswordEncoder passwordEncoder;

    @Mock
    UserDetailsManager userDetailsManager;

    @Mock
    PlaylistsService playlistsService;

    @InjectMocks
    UsersInfoServiceImpl mockUsersInfoService;

    @Test
    public void findByUsernameShould_ReturnedUserWithSameUsername() {
        // Arrange
        UserInfo expected = createUserInfo();
        Mockito.when(usersInfoRepository.findByUsername("testUsername"))
                .thenReturn(expected);
        // Act
        UserInfo returned = mockUsersInfoService.findByUsername("testUsername");
        // Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void findByEmailShould_ReturnedUserWithSameEmail() {
        // Arrange
        UserInfo expected = createUserInfo();
        Mockito.when(usersInfoRepository.findByEmail("testEmail@test.com"))
                .thenReturn(expected);
        // Act
        UserInfo returned = mockUsersInfoService.findByEmail("testEmail@test.com");
        // Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void findUserByIdShould_ReturnedUserWithSameId() {
        // Arrange
        UserInfo expected = createUserInfo();
        Mockito.when(usersInfoRepository.findUserById(1L))
                .thenReturn(expected);
        // Act
        UserInfo returned = mockUsersInfoService.findUserById(1L);
        // Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void findAllShould_ReturnedAllUsers() {
        // Arrange
        UserInfo expected = createUserInfo();
        Mockito.when(usersInfoRepository.findAll())
                .thenReturn(Collections.singletonList(expected));
        // Act
        List<UserInfo> returned = mockUsersInfoService.findAll();
        // Assert
        Assert.assertSame(1, returned.size());
    }

    @Test
    public void saveShould_SaveUser_WhenUserNotExists() {
        // Arrange
        User user = createUser();
        Mockito.when(passwordEncoder.encode(anyString()))
                .thenReturn("testPassword");
        // Act
        mockUsersInfoService.save(user);
        // Assert
        ArgumentCaptor<UserInfo> userArgument = ArgumentCaptor.forClass(UserInfo.class);
        Mockito.verify(usersInfoRepository, times(1)).save(userArgument.capture());
        UserInfo createdUser = userArgument.getValue();
        Mockito.verify(usersInfoRepository,
                times(1)).save(createdUser);
    }

    @Test
    public void saveShould_ThrowException_WhenUserWithSameNameAlreadyExists() {
        // Arrange
        User expected = createUser();
        Mockito.when(userDetailsManager.userExists(expected.getUsername()))
                .thenReturn(true);
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockUsersInfoService.save(expected));
    }

    @Test
    public void saveShould_ThrowException_WhenUserWithSameEmailAlreadyExists() {
        // Arrange
        User expected = createUser();
        Mockito.when(usersInfoRepository.findByEmail(expected.getEmail()))
                .thenReturn(createUserInfo());
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockUsersInfoService.save(expected));
    }

    @Test
    public void updateShould_UpdateUser_WhenUserExists() {
        // Arrange
        UserInfo expected = createUserInfo();
        Mockito.when(usersInfoRepository.findUserById(1L))
                .thenReturn(expected);
        // Act
        mockUsersInfoService.update(expected, 1L);
        // Assert
        ArgumentCaptor<UserInfo> userArgument = ArgumentCaptor.forClass(UserInfo.class);
        Mockito.verify(usersInfoRepository, times(1)).save(userArgument.capture());
        UserInfo updatedUser = userArgument.getValue();
        Mockito.verify(usersInfoRepository,
                times(1)).save(updatedUser);
    }

    @Test
    public void updateShould_ThrowException_WhenUserNotExists() {
        // Arrange
        UserInfo expected = createUserInfo();
        Mockito.when(userDetailsManager.userExists(expected.getUsername()))
                .thenReturn(true);
        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUsersInfoService.update(expected, 1L));
    }

    @Test
    public void updateShould_ThrowException_WhenUserWithSameEmailAlreadyExists() {
        // Arrange
        UserInfo expected = createUserInfo();
        Mockito.when(usersInfoRepository.findByEmail(expected.getEmail()))
                .thenReturn(createUserInfo());
        UserInfo user = new UserInfo();
        user.setEmail("testOtherEmail@test.bg");
        Mockito.when(usersInfoRepository.findUserById(1L))
                .thenReturn(user);
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockUsersInfoService.update(expected, 1L));
    }

    @Test
    public void changePasswordShould_CallUserDetailsManager() {
        // Arrange
        User user = createUser();
        Mockito.when(passwordEncoder.encode(anyString()))
                .thenReturn("newTestPassword");
        // Act
        mockUsersInfoService.changePassword(user.getPassword(), "newTestPassword");
        // Assert
        Mockito.verify(userDetailsManager, times(1))
                .changePassword(user.getPassword(), "newTestPassword");
    }

    @Test
    public void deleteByIdShould_DeleteUser_WhenUserExists() {
        // Arrange
        Mockito.when(usersInfoRepository.existsById(anyLong()))
                .thenReturn(true);
        Mockito.when(usersInfoRepository.findUserById(anyLong()))
                .thenReturn(createUserInfo());
        // Act
        mockUsersInfoService.deleteById(1L);
        // Assert
        Mockito.verify(usersInfoRepository, times(1))
                .deleteById(1L);
        Mockito.verify(userDetailsManager, times(1))
                .deleteUser(anyString());
    }

    @Test
    public void deleteByIdShould_DeleteUserPlaylists() {
        // Arrange
        Mockito.when(usersInfoRepository.existsById(anyLong()))
                .thenReturn(true);
        Mockito.when(usersInfoRepository.findUserById(anyLong()))
                .thenReturn(createUserInfo());
        // Act
        mockUsersInfoService.deleteById(1L);
        // Assert
        Mockito.verify(playlistsService, times(1))
                .deletePlaylist(anyLong());
    }

    @Test
    public void deleteByIdShould_ThrowException_WhenUserNotExists() {
        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUsersInfoService.deleteById(1L));
    }

    @Test
    public void getPlaylistsOfUserShould_ReturnedPlaylistsOfUser() {
        // Arrange
        UserInfo expected = createUserInfo();
        Mockito.when(usersInfoRepository.findUserById(anyLong()))
                .thenReturn(expected);
        // Act
        Set<Playlist> returned = mockUsersInfoService.getPlaylistsOfUser(anyLong());
        // Assert
        Assert.assertSame(1, returned.size());
    }
}
