package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.exceptions.DuplicateEntityException;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Artist;
import com.telerikacademy.ridepal.repositories.ArtistsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static com.telerikacademy.ridepal.Factory.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class ArtistsServiceImplTests {
    @Mock
    ArtistsRepository artistsRepository;

    @InjectMocks
    ArtistsServiceImpl mockArtistsService;

    @Test
    public void findByNameShould_ReturnedArtistWithSameName() {
        // Arrange
        Artist expected = createArtist();
        Mockito.when(artistsRepository.findByName(expected.getName()))
                .thenReturn(expected);
        // Act
        Artist returned = mockArtistsService.findByName(expected.getName());
        // Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void findArtistByIdShould_ReturnedArtistWithSameId() {
        // Arrange
        Artist expected = createArtist();
        Mockito.when(artistsRepository.findArtistById(1L))
                .thenReturn(expected);
        // Act
        Artist returned = mockArtistsService.findArtistById(1L);
        // Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void findAllShould_ReturnedAllArtists() {
        // Arrange
        Artist expected = createArtist();
        Mockito.when(artistsRepository.findAll())
                .thenReturn(Collections.singletonList(expected));
        // Act
        List<Artist> returned = mockArtistsService.findAll();
        // Assert
        Assert.assertSame(1, returned.size());
    }

    @Test
    public void saveShould_SaveArtist_WhenArtistNotExists() {
        // Arrange
        Artist expected = createArtist();
        // Act
        mockArtistsService.save(expected);
        // Assert
        Mockito.verify(artistsRepository, times(1))
                .save(expected);
    }

    @Test
    public void saveShould_ThrowException_WhenArtistExists() {
        // Arrange
        Artist expected = createArtist();
        Mockito.when(artistsRepository.existsByName(expected.getName()))
                .thenReturn(true);
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockArtistsService.save(expected));
    }

    @Test
    public void updateShould_UpdateArtist_WhenArtistExists() {
        // Arrange
        Artist expected = createArtist();
        Mockito.when(artistsRepository.existsById(1L))
                .thenReturn(true);
        Mockito.when(artistsRepository.findArtistById(1L))
                .thenReturn(expected);
        // Act
        mockArtistsService.update(expected, 1L);
        // Assert
        Mockito.verify(artistsRepository, times(1))
                .save(expected);
    }

    @Test
    public void updateShould_ThrowException_WhenArtistNotExists() {
        // Arrange
        Artist expected = createArtist();
        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockArtistsService.update(expected, 1L));
    }

    @Test
    public void updateShould_ThrowException_WhenArtistsWithNameAlreadyExists() {
        // Arrange
        Artist expected = createArtist();
        Mockito.when(artistsRepository.existsById(1L))
                .thenReturn(true);
        Mockito.when(artistsRepository.existsByName(expected.getName()))
                .thenReturn(true);
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockArtistsService.update(expected, 1L));
    }

    @Test
    public void deleteByIdShould_DeleteArtist_WhenArtistExists() {
        // Arrange
        Mockito.when(artistsRepository.existsById(1L))
                .thenReturn(true);
        // Act
        mockArtistsService.deleteById(1L);
        // Assert
        Mockito.verify(artistsRepository, times(1))
                .deleteById(1L);
    }

    @Test
    public void deleteByIdShould_ThrowException_WhenArtistNotExists() {
        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockArtistsService.deleteById(1L));
    }

    @Test
    public void existsByNameShould_CallRepository() {
        // Arrange
        Mockito.when(artistsRepository.existsByName(anyString()))
                .thenReturn(true);
        // Act
        mockArtistsService.existsByName(anyString());
        // Assert
        Mockito.verify(artistsRepository, times(1))
                .existsByName(anyString());
    }
}
